<?php

require_once "contact.php";

if ($validated_data === false) {
    $response['error']=true;
    $response['validation_errors']=$validator->get_readable_errors();
} else {
    // validation successful
    $contact_data = "{$validated_data['name']}, {$validated_data['email']}, {$validated_data['phone']}";
    
    // Creating an Instance
    $fileHelper = new FileHelper('contacts.txt');

    //print_r($fileHelper);
    // writing
    if ($fileHelper->deleteLine($contact_data)) {
       $response['data'] = "Contact Deleted";
    }else{
        $response['error']=true;
        $response['error_message']="Error deleting contact! check that you have write permissions to the project directory";
    }
}
echo json_encode($response);