<?php

class FileHelper
{

    /**
     * Use to store fOpen connection
     */
    private $handle;

    /**
     * To store the file location
     */
    private $file;

    public function __construct($file_url)
    {
        $this->file = $file_url;
    }

    /**
     * Used to initialize the file
     * @access public
     * @param string $file_url
     * File location/url
     * @return bool
     */
    public function load($file_url)
    {
        $this->file = $file_url;
        if ($this->handle = fopen($this->file, 'a+')) {
            return $this;
        }else{
            echo "not opened";
        }
    }

    /**
     * Used to write inside the file,
     * If file doesn't exists it will create it
     * @access public
     * @param string $text
     * The text which we have to write
     * @example 'My text here in the file.';
     * @return bool
     */
    public function write($text)
    {
        return file_put_contents($this->file, $text, FILE_APPEND);
    }

    /**
     * To read the contents of the file
     * @access public
     * @param bool $nl2br
     * By default set to false, if set to true will return
     * the contents of the file by preserving the data.
     * @example (true)
     * @return string|bool
     */
    public function read()
    {
       return file_get_contents($this->file);
    }

    /**
     * Use to delete the file
     * @access public
     * @return bool
     */
    public function delete()
    {
        fclose($this->handle);

        if (file_exists($this->file)) {
            if (unlink($this->file)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function deleteLine($line)
    {
        $old_contents = file_get_contents($this->file);
        $new_contents = str_replace($line, '', $old_contents);
        file_put_contents($this->file, $new_contents);
        return ($old_contents != $new_contents);
    }
}

/* HOW TO USE */

// // Sample text
// $text = <<<file
// Name: Ashwin Pathak
// Age: 15 years
// Country: India
// Blog Url: http://codicious.blogspot.com
// file;

// // Creating an Instance
// $file = new File;

// // writing
// if ($file->load('text.txt')->write($text)) {
//     echo '<b>Status: </b>Wrote successfully!<br /><br />';
// }

// // reading
// if ($read = $file->load('text.txt')->read(true)) {
//     echo '<b>Status: Reading</b><br />' . $read;
// }

// // deleting
// if ($file->load('text.txt')->delete()) {
//     echo '<br /><br /><b>Status: </b>Deleted successfully!';
// }

// /*
//     Post URL:   http://codicious.blogspot.com
//  */