$(document).ready(function () {
    $('.add-contact-btn').click(function () {
        $('.add-contact').removeClass('hide');
    });

    $('.save-contact-btn').click(function () {
        $('input').attr("data-validation-optional", "true");
        $('form#addContactForm').submit();
    });

    $('form#addContactForm').submit(function (e) {
        e.preventDefault();
        var savebtn = $('button.save-contact-btn');
        var v_container = $('div.add-contact-validation ul');
        savebtn.val('Saving ....');
        sendToServer("addContact.php", $(this).serialize(), v_container, savebtn);
    });

    $('form#removeContactForm').submit(function (e) {
        e.preventDefault();
        var removebtn = $('button.remove-contact-btn');
        var v_container = $('div.remove-contact-validation ul');
        removebtn.val('Removing ....');
        sendToServer("removeContact.php", $(this).serialize(), v_container, removebtn);
    });

    $('#remove-contact-btn').click(function () {
        $('form#removeContactForm').submit();
    });

    $('.validate-contact-btn').click(function () {
        $('input').attr("data-validation-optional", "false");
        $('form#addContactForm').validate();
    });


    $.validate({
        language: 'en'
    });

    var sendToServer = function (url, formdata, v_container, actionBtn) {
        $.post(url, formdata)
            .done(function (data) {
                var result = JSON.parse(data);
                var li = "";
                v_container.html("");
                if (result.error) {
                    if (result.validation_errors.length > 0) {
                        $.each(result.validation_errors, function (index, value) {
                            li += "<li>" + value + "</li>";
                        });
                    }
                    if (result.error_message) {
                        li += "<li>" + result.error_message + "</li>";
                    }
                    v_container.html(li);
                } else {
                    $('div.add-contact-success').text("Operation Successfully");

                    if(url=="removeContact.php"){
                        $('form#removeContactForm')[0].reset();
                    }
                }
            })
            .fail(function (err) {
                console.log(err);
            })
            .always(function () {
                actionBtn.val('Save');
            });
    }
});