
<?php
require_once "validation.php";
require_once "file.php";
$response = array(
    'error'=>false,
    'error_message'=>'',
    'data'=>array(),
    'validation_errors'=>array()
);
$validator = new FormValidation();
// never trust a user haha!!
$_POST = $validator->sanitize($_POST);
$validator->validation_rules(array(
    'name' => 'required|alpha_space',
    'email' => 'required|valid_email',
    'phone' => 'required|numeric',
));
$validator->filter_rules(array(
    'name' => 'trim|sanitize_string',
    'email' => 'trim|sanitize_email',
    'phone' => 'trim'
));
$validated_data = $validator->run($_POST);
?>