<?php
require_once ('Config.php');

$searchTerm = $_POST['searchTerm'];
$response = array(
    'error'=>false,
    'error_message'=>'',
    'data'=>array()
);

if(!isset($searchTerm) || $searchTerm == ""){
    $response['error']=true;
    $response['error_message']="Please enter a search term";
}

$db=new Db(); 
$response['data'] = $db->searchFilename(["file", "directory"], $searchTerm);

if(count($response['data'])>0){
    for($i=0;$i<count($response['data']); $i++){
        $response['data'][$i]->path = $db->getFilePath($response['data'][$i]);
    }
}

echo json_encode($response);
?>