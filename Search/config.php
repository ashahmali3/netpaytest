<?php
ini_set( "display_errors", true );
define( "DB_DSN", "mysql:host=localhost;dbname=netpay_filesystem" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "" );
define('DB_CHARACSET', 'utf8');
require_once ('Db.php');

function handleException( $exception ) {
  echo  $exception->getMessage();
}
set_exception_handler( 'handleException' );

?>