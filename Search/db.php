<?php

/**
 * PDO mysql database class
 */
class Db
{
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '" . DB_CHARACSET . "';"));
            $this->pdo->exec("SET CHARACTER SET " . DB_CHARACSET);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->query("set names " . DB_CHARACSET);
        } catch (PDOException $e) {
            echo "error " . $e->getMessage();
        }
    }
    /**
     * custom query ,update,delete,insert,or fetch, joining multiple table etc, aritmathic etc
     * @param  string $sql  custom query
     * @param  array $data associative array
     * @return array  recordset
     */
    public function custom_query($sql, $data = null)
    {
        if ($data !== null) {
            $dat = array_values($data);
        }
        $sel = $this->pdo->prepare($sql);
        if ($data !== null) {
            $sel->execute($dat);
        } else {
            $sel->execute();
        }
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel;
    }

    /**
     * fetch only one row
     * @param  string $table table name
     * @param  string $col condition column
     * @param  string $val value column
     * @return array recordset
     */
    public function fetch_single_row($table, $col, $val)
    {
        $nilai = array($val);
        $sel = $this->pdo->prepare("SELECT * FROM $table WHERE $col=?");
        $sel->execute($nilai);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        $obj = $sel->fetch();
        return $obj;
    }
    /**
     * fetch all data
     * @param  string $table table name
     * @return array recordset
     */
    public function fetch_all($table)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM $table");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $stmt->fetchAll();
    }

    /**
     * fetch multiple row
     * @param  string $table table name
     * @param  array $dat specific column selection
     * @return array recordset
     */
    public function fetch_col($table, $dat)
    {
        if ($dat !== null)
            $cols = array_values($dat);
        $col = implode(', ', $cols);
        $sel = $this->pdo->prepare("SELECT $col from $table");
        $sel->execute();
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel;
    }
    /**
     * fetch row with condition
     * @param  string $table table name
     * @param  array $col which columns name would be select
     * @param  array $where what column will be the condition
     * @return array recordset
     */
    public function fetch_multi_row($table, $col, $where)
    {
        $data = array_values($where);
        //grab keys
        $cols = array_keys($where);
        $colum = implode(', ', $col);
        foreach ($cols as $key) {
            $keys = $key . "=?";
            $mark[] = $keys;
        }
        $jum = count($where);
        if ($jum > 1) {
            $im = implode('? and  ', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        } else {
            $im = implode('', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        }
        $sel->execute($data);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel;
    }
    /**
     * fetch row with condition
     * @param  string order ASC or DESC
     * @param  string $index based on which column
     * @param  string $table table name
     * @param  array $col which columns name would be select
     * @param  array $where what column will be the condition
     * @return array recordset
     */
    public function fetch_multi_row_order($table, $col, $where, $order, $index)
    {
        $data = array_values($where);
        //grab keys
        $cols = array_keys($where);
        $colum = implode(', ', $col);
        foreach ($cols as $key) {
            $keys = $key . "=?";
            $mark[] = $keys;
        }
        $jum = count($where);
        if ($jum > 1) {
            $im = implode('? and  ', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im ORDER BY $index $order");
        } else {
            $im = implode('', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im ORDER BY $index $order");
        }
        $sel->execute($data);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel;
    }
    /**
     * check if there is exist data
     * @param  string $table table name
     * @param  array $dat array list of data to find
     * @return true or false
     */
    public function check_exist($table, $dat)
    {
        $data = array_values($dat);
       //grab keys
        $cols = array_keys($dat);
        $col = implode(', ', $cols);
        foreach ($cols as $key) {
            $keys = $key . "=?";
            $mark[] = $keys;
        }
        $jum = count($dat);
        if ($jum > 1) {
            $im = implode(' and  ', $mark);
            $sel = $this->pdo->prepare("SELECT $col from $table WHERE $im");
        } else {
            $im = implode('', $mark);
            $sel = $this->pdo->prepare("SELECT $col from $table WHERE $im");
        }
        $sel->execute($data);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        $jum = $sel->rowCount();
        if ($jum > 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * search data
     * @param  string $table table name
     * @param  array $col   column name
     * @param  array $where where condition
     * @return array recordset
     */
    public function search($table, $col, $where)
    {
        $data = array_values($where);
        foreach ($data as $key) {
            $val = '%' . $key . '%';
            $value[] = $val;
        }
       //grab keys
        $cols = array_keys($where);
        $colum = implode(', ', $col);
        foreach ($cols as $key) {
            $keys = $key . " LIKE ?";
            $mark[] = $keys;
        }
        $jum = count($where);
        if ($jum > 1) {
            $im = implode(' OR  ', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        } else {
            $im = implode('', $mark);
            $sel = $this->pdo->prepare("SELECT $colum from $table WHERE $im");
        }
        $sel->execute($value);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel;
    }

    /**
     * search data
     * @param  string $table table name
     * @param  array $col   column name
     * @param  array $where where condition
     * @return array recordset
     */
    public function searchFilename($tables, $searchTerm)
    {
        $searchTerm = "%" . $searchTerm . "%";
        $sql = "SELECT id, name, parent_directory_id FROM $tables[0] WHERE name LIKE ? UNION ALL ";
        $sql .= "SELECT id, name, parent_directory_id FROM $tables[1] WHERE name LIKE ?";
        $sel = $this->pdo->prepare($sql);
        $sel->execute([$searchTerm, $searchTerm]);
        $sel->setFetchMode(PDO::FETCH_OBJ);
        return $sel->fetchAll();
    }

    /**
     * search data
     * @param  string $table table name
     * @param  array $col   column name
     * @param  array $where where condition
     * @return array recordset
     */
    public function getFilePath($file)
    {
        $nested = true;
        $path = "";
        $parentDir = $file->parent_directory_id;
        do{
            $sql = "SELECT name, parent_directory_id FROM directory WHERE id = ?";
            $sel = $this->pdo->prepare($sql);
            $sel->execute([$parentDir]);
            $sel->setFetchMode(PDO::FETCH_OBJ);
            $result = $sel->fetchAll();
            $path=$result[0]->name."/".$path;
            $parentDir = $result[0]->parent_directory_id;
            if($parentDir<=0){ $nested=false; }
        }
        while($parentDir>0);
        return $path.$file->name;
    }

    /**
     * get last insert id
     * @return int last insert id
     */
    public function get_last_id()
    {
        return $this->pdo->lastInsertId();
    }
    /**
     * insert data to table
     * @param  string $table table name
     * @param  array $dat   associative array 'column_name'=>'val'
     */
    public function insert($table, $dat)
    {
        if ($dat !== null)
            $data = array_values($dat);
        //grab keys
        $cols = array_keys($dat);
        $col = implode(', ', $cols);
        //grab values and change it value
        $mark = array();
        foreach ($data as $key) {
            $keys = '?';
            $mark[] = $keys;
        }
        $im = implode(', ', $mark);
        $ins = $this->pdo->prepare("INSERT INTO $table ($col) values ($im)");
        $ins->execute($data);
    }
 
  
    public function __destruct()
    {
        $this->pdo = null;
    }
}
?>