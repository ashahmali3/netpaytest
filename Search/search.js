$(document).ready(function () {
    $('form#searchForm').submit(function (e) {
        var searchbtn = $('input#submitBtn');
        var resultDiv = $('div#search-result');
        e.preventDefault();
        searchbtn.val('Searching ....');
        $.post("search.php", $(this).serialize())
            .done(function (data) {
                var result = JSON.parse(data);
                resultDiv.text("").append("<h3>Search Result</h3>");
                if (result.error) {
                    $('div#errors').text(result.error_message);
                }
                if (result.data.length > 0) {
                    $.each(result.data, function (index, value) {
                        resultDiv.append(
                            "<p>" + value.path + "</p>"
                        );
                    });
                } else {
                    resultDiv.append(
                        "<p>No Result</p>"
                    );
                }
            })
            .fail(function (err) {
                console.log(err);
            })
            .always(function () {
                searchbtn.val('Search');
            });
    });

});

