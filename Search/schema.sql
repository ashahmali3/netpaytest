- -----------------------------------------------------
-- Schema netpay_filesystem
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `netpay_filesystem` DEFAULT CHARACTER SET utf8 ;
USE `netpay_filesystem` ;

-- -----------------------------------------------------
-- Table `netpay_filesystem`.`directory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpay_filesystem`.`directory` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `parent_directory_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `netpay_filesystem`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netpay_filesystem`.`file` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `size` INT NOT NULL,
  `parent_directory_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `parent_directory_id`),
  INDEX `fk_file_directory_idx` (`parent_directory_id` ASC),
  CONSTRAINT `fk_file_directory`
    FOREIGN KEY (`parent_directory_id`)
    REFERENCES `netpay_filesystem`.`directory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
